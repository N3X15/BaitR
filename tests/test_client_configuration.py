from ipaddress import ip_address
import unittest

from baitr.client_configuration import ClientConfiguration
from nacl.public import PrivateKey

class ClientConfigurationTests(unittest.TestCase):
    def test_client_ipv4_toBytes(self) -> None:
        cli = ClientConfiguration()
        cli.id = 'abcdefghijklmnop'
        cli.hostname = 'pc-shitbox'
        cli.ip_addr = ip_address('1.2.3.4')
        cli.mac_addr = bytes([1,2,3,4,5,6])
        cli.pk = PrivateKey(b'\xAA'*PrivateKey.SIZE)
        cli.port = 6667

        b = cli.asBytes()
        expected = b'\x02' # version
        expected += b'\x10' # len(id)
        expected += b'abcdefghijklmnop' # id
        expected += b'\x0A' # len(hostname)
        expected += b'pc-shitbox' #hostname
        expected += b'\x84\x86\x88\x08' # ip
        expected += b'\x8b4'  # port
        expected += b'\x01\x02\x03\x04\x05\x06' #mac
        expected += b' ' # len(pk)
        expected += (b'\xAA'*PrivateKey.SIZE)

        self.assertEqual(b, expected)

    def test_client_ipv4_fromBytes(self) -> None:
        b = b'\x02'  # version
        b += b'\x10'  # len(id)
        b += b'abcdefghijklmnop'  # id
        b += b'\x0A'  # len(hostname)
        b += b'pc-shitbox'  # hostname
        b += b'\x84\x86\x88\x08'  # ip
        b += b'\x8b4'  # port
        b += b'\x01\x02\x03\x04\x05\x06'  # mac
        b += b' '  # len(pk)
        b += (b'\xAA' * PrivateKey.SIZE)

        cli = ClientConfiguration()
        cli.fromBytes(b)
        self.assertEqual(cli.id, 'abcdefghijklmnop')
        self.assertEqual(cli.hostname, 'pc-shitbox')
        self.assertEqual(cli.ip_addr, ip_address('1.2.3.4'))
        self.assertEqual(cli.mac_addr, bytes([1, 2, 3, 4, 5, 6]))
        self.assertEqual(bytes(cli.pk), b'\xAA' * PrivateKey.SIZE)
        self.assertEqual(cli.port, 6667)
