# @generated
import enum
__ALL__ = ['EFloatFlags']
class EFloatFlags(enum.IntFlags):
    NONE = 0
    NAN = 1
    POS_INFINITY = 4
    NEG_INFINITY = 2
'''
 b00000000000000000000000000000111
0x7
'''
EFloatFlags.ALL = 7
