# @generated
import enum
__ALL__ = ['EDataType']
class EDataType(enum.IntEnum):
    BOOL = 13
    BYTES = 10
    FLOAT = 7
    INT = 3
    INT16 = 4
    INT32 = 1
    INT64 = 0
    INT8 = 9
    OBJ = 12
    STR = 8
    UINT16 = 6
    UINT32 = 5
    UINT64 = 11
    UINT8 = 2
EDataType.MIN = 0
EDataType.MAX = 13
EDataType.WIDTH = 4
