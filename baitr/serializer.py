from enum import Enum, IntEnum, IntFlag, auto
from io import BytesIO
import math
import struct
from typing import BinaryIO, Optional
from baitr.enums.datatype import EDataType

class EFloatFlags(IntFlag):
    NONE = 0
    NAN = auto()
    POS_INFINITY = auto()
    NEG_INFINITY = auto()
EFloatFlags.WIDTH = EFloatFlags.NEG_INFINITY.value.bit_length()

class BaitRSerializer:
    def __init__(self, w: Optional[BinaryIO], annotate: bool = True) -> None:
        self.buffer: int = 0
        self.bits: int = 0
        self.writer: BinaryIO = w or BytesIO()
        self.annotate: bool = annotate

    def _add_int(self, val: int, bits: int) -> None:
        self.buffer = (val << bits) | val
        self.bits += bits
        self._collect()

    def _add_bytes(self, val: bytes, bits: int = -1) -> None:
        bitsleft: int = bits
        if bitsleft == -1:
            bitsleft = len(val) * 8
        for c in val:
            curbits = min(bitsleft, 8)
            self.buffer = (val << curbits) | val
            self.bits += curbits
            bitsleft -= curbits
            self._collect()

    def _add_annotation(self, val: EDataType) -> None:
        if self.annotate:
            self.add_int(val.value, EDataType.WIDTH)

    # 7-bit encoding
    def _add_int7(self, val: int, bits: int = -1) -> None:
        if bits == -1:
            bits = val.bit_length()
        while val >= 0x80:
            self._add_int(val | 0x80, 8)
            val >>= 7
        self._add_int(val, 8)

    def add_int(self, val: int, bits: int = -1) -> None:
        self._add_annotation(EDataType.INT7)
        self._add_int7(val, bits)

    def add_int8(self, val: int) -> None:
        self._add_annotation(EDataType.INT8)
        self._add_bytes(val.to_bytes(1, 'big', signed=True))

    def add_int16(self, val: int) -> None:
        self._add_annotation(EDataType.INT16)
        self._add_bytes(val.to_bytes(2, 'big', signed=True))

    def add_int32(self, val: int) -> None:
        self._add_annotation(EDataType.INT32)
        self._add_bytes(val.to_bytes(4, 'big', signed=True))

    def add_int64(self, val: int) -> None:
        self._add_annotation(EDataType.INT64)
        self._add_bytes(val.to_bytes(8, 'big', signed=True))

    def add_uint8(self, val: int) -> None:
        self._add_annotation(EDataType.UINT8)
        self._add_bytes(val.to_bytes(1, 'big', signed=False))

    def add_uint16(self, val: int) -> None:
        self._add_annotation(EDataType.UINT16)
        self._add_bytes(val.to_bytes(2, 'big', signed=False))

    def add_uint32(self, val: int) -> None:
        self._add_annotation(EDataType.UINT32)
        self._add_bytes(val.to_bytes(4, 'big', signed=False))

    def add_uint64(self, val: int) -> None:
        self._add_annotation(EDataType.UINT64)
        self._add_bytes(val.to_bytes(8, 'big', signed=False))

    def add_str(self, val: str) -> None:
        self._add_annotation(EDataType.STR)
        self.add_bytes(val.encode('utf-8'))

    def add_bytes(self, val: bytes) -> None:
        self._add_annotation(EDataType.BYTES)
        self._add_int7(len(val))
        self._add_bytes(val)

    def add_float(self, val: float) -> None:
        self._add_annotation(EDataType.FLOAT)
        flags = EFloatFlags.NONE
        if math.isnan(val):
            flags |= EFloatFlags.NAN
        if math.isinf(val):
            if val > 0:
                flags |= EFloatFlags.POS_INFINITY
            else:
                flags |= EFloatFlags.NEG_INFINITY
        self._add_int(flags.value, EFloatFlags.WIDTH)
        if flags == EFloatFlags.NONE:
            self._add_bytes(struct.pack('d', val))

    def add_bool(self, val: bool) -> None:
        self._add_annotation(EDataType.BOOL)
        self._add_int(1 if val else 0, 1)

    def _collect(self) -> None:
        if self.bits >= 8:
            while self.bits >= 8:
                self.buffer >>= 8
                c = self.buffer & 0xff
                self.writer.write(c)

    def finish(self) -> None:
        if self.bits > 0:
            c = (self.buffer >> self.bits) & ((1 << self.bits) - 1)
            self.writer.write(c)
        
    def as_bytes(self) -> None:
        if isinstance(self.writer, BytesIO):
            return self.writer.getvalue()

    