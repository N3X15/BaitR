import pickle
from io import BytesIO
from pathlib import Path
from typing import Dict, Optional

import nacl.utils
from baitr.client_configuration import ClientConfiguration
from baitr.comms.binary import read_u4, read_u8, write_u4, write_u8
from nacl.public import PrivateKey
from nacl.pwhash.argon2i import SALTBYTES
from nacl.pwhash.argon2i import kdf as Argon2IKDF
from nacl.secret import SecretBox


class KeyMaster:
    '''
    This is the C2C server key management system.

    Keys are stored as a table on disk and encrypted with a master key.

    Yes, we have the private keys for the clients, because.
    '''
    
    KMFILE = Path.cwd() / '.kmdb'
    KMFILE_VERSION = 1

    def __init__(self) -> None:
        self.c2c_key: Optional[PrivateKey] = None
        self.clients: Dict[str, ClientConfiguration] = {}
        self.salt: Optional[bytes] = None
        self.key: Optional[bytes] = None
        
    def saveTo(self, filename: Path, password: bytes) -> None:
        if self.salt is None or self.key is None:
            self.salt = nacl.utils.random(SALTBYTES)
            self.key = Argon2IKDF(SecretBox.KEY_SIZE, password, self.salt)
        box = SecretBox(self.key)

        data = b''
        def appendBytes(b: bytes) -> None:
            nonlocal data
            data += len(b).to_bytes(4, 'big', signed=False) + b

        appendBytes(bytes(self.c2c_key))
        appendBytes(pickle.dumps(self.clients, protocol=pickle.HIGHEST_PROTOCOL))

        encrypted = box.encrypt(data)
        with filename.open('wb') as f:
            write_u4(self.KMFILE_VERSION, f)
            write_u4(len(self.salt), f)
            f.write(self.salt)

            write_u8(len(encrypted), f)
            f.write(encrypted)

    def loadFrom(self, filename: Path, password: bytes) -> None:
        with filename.open('rb') as f:
            assert read_u4(f) == self.KMFILE_VERSION, 'Invalid version'
            self.salt: bytes = f.read(read_u4(f))
            self.key = Argon2IKDF(SecretBox.KEY_SIZE, password, self.salt)
            box = SecretBox(self.key)
            encrypted = f.read(read_u8(f))
            data = box.decrypt(encrypted)
        bio = BytesIO(data)
        self.c2c_key = PrivateKey(bio.read(read_u4(bio)))
        self.clients = pickle.loads(bio.read(read_u4(bio)))
