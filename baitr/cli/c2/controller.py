from enum import IntEnum
import logging
from queue import Queue
import socket
from threading import Thread
import threading
from typing import Dict, Optional, Tuple

from construct import Container

from baitr.client_configuration import ClientConfiguration
from baitr.comms.packets.client_banner import ClientBanner


class EClientConnectionState(IntEnum):
    NOT_CONNECTED = 0
    WAITING_FOR_HANDSHAKE = 1
    HANDSHAKING = 2
    DISABLED = 3
    ERROR = 4
    CONNECTED = 5
    CONNECTION_LOST = 6


log = logging.getLogger(__qualname__)
class ConnectedClient(Thread):
    def __init__(self, cc: ClientConfiguration) -> None:
        self.identity: ClientConfiguration = cc
        self.client_version: Tuple[int, int, int] = (0, 0, 0)
        self.sent: int = 0
        self.recv: int = 0
        self.state: EClientConnectionState = EClientConnectionState.NOT_CONNECTED
        self.socket: socket.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.send_queue: Queue = Queue()
        
    def connect(self) -> None:
        log.info(f'Connecting to {self.identity.ip_addr}:{self.identity.port} ({self.identity.id})')
        threading.Thread(target=self._run_commsloop, args=[])

    def badClientMessage(self, msg: str, data: bytes, parsed: Optional[Container] = None) -> None:
        log.error(msg)
        log.error(f'  Data (Raw):    {data!r}')
        if parsed is not None:
            log.error(f'  Data (Parsed): {parsed}')

    def _run_commsloop(self) -> None:
        with self.socket:
            self.socket.connect((self.identity.ip_addr, self.identity.port))
            while True:
                # Begin handshake (Check banner)
                rawdata = self.socket.recv(1024)
                if self.state == EClientConnectionState.HANDSHAKING:
                    banner = ClientBanner.parse(rawdata)
                    if banner.proto_version != PROTO_VERSION:
                        self.badClientMessage(f'Agent {self.identity.id!r} uses invalid protocol number {banner.proto_version}. Disconnecting.', rawdata, banner)
                        return

class C2Controller:
    def __init__(self) -> None:
        self.clients: Dict[str, ConnectedClient] = {}
        cli = ConnectedClient(ClientConfiguration())
        cli.run()
