# This blob just holds current state information.  Actual configuration is hardcoded.
class Configuration:
    def __init__(self) -> None:
        self.id: int = 0
        self.debug: bool = False
        self.randomly_freeze_remote_users: bool = False
        self.replaced_systools: bool = False