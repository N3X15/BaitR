import os
import random
import socket
import string
import sys
import time
import uuid
from ipaddress import _IPAddressBase, ip_address

import click

import logging

from .agent_consts import MY_ADDR, MY_HOST, MY_NODE

log = logging.getLogger(__name__)

def environment_matches() -> bool:
    if uuid.getnode() != MY_NODE:
        return False
    if socket.gethostname() != MY_HOST:
        return False
    if ip_address(socket.gethostbyname(socket.gethostname())) != MY_ADDR:
        return False
    return True

def main() -> None:
    if not Configuration.exists():
        # Fuck you "John", we're gonna warn grandma and you can't do shit about it.
        disconnectRemoteUsers()

        click.secho('!!! HEY YOU, READ THIS !!!', fg='red')
        click.secho()
        click.secho('You have opened BaitR, a tool that gives FULL CONTROL of your computer to OTHER PEOPLE.', fg='red')
        click.secho()
        click.secho('If you were asked to open this by someone, close this window now!')
        click.secho()
        click.secho('BaitR is intended for the purpose of trolling tech support scammers in a controlled environment by trained (and bored) security professionals.')
        click.secho()
        click.secho('It is NOT designed for illegal activities and has measures to prevent its use by bad actors.')
        click.secho("It is NOT designed for use by a remote \"tech\", \"banker\", \"officer\", \"IRS agent\", etc. Those are probably scammers or idiots and you should hang up")
        click.secho('BaitR IS a powerful tool for someone who knows what they\'re doing.')
        click.secho()

        key = random.choices(list(string.ascii_letters+string.digits), k=10)

        click.secho("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
        click.secho("! IF YOU DON'T WANT THIS OR DON'T UNDERSTAND WHAT IS GOING !")
        click.secho("!    ON, HOLD DOWN THE CTRL KEY AND PRESS c TO EXIT NOW!   !")
        click.secho("!           (You can also just close the window)           !")
        click.secho("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
        click.secho()
        click.secho('Pausing for 5s to give you a chance to read...')
        time.sleep(5.)
        click.secho('If you wish to GIVE SOMEONE FULL CONTROL OF YOUR PC, please press enter the following code (case-sensitive):')
        click.secho(f' {key}')
        if click.prompt('Confirmation Code>') != key:
            click.secho('[*] Exiting...', fg='green')
            sys.exit(1)

        if not environment_matches():
            log.debug('lol no')
            sys.exit(0)

        cfg = Configuration()



if __name__ == "__main__":
    main()
