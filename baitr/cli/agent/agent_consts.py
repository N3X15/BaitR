# This file gets overwritten pre-compile.
import datetime

MY_NODE: int  = 0x0000
MY_HOST: str = ''
MY_ADDR: int  = 0x0001
MY_PORT: int  = 0x0002
MY_NAME: str  = 'name'
MY_KEY: bytes = b'key'
COMPILED_BY: str = ''
COMPILED_AT: datetime.datetime = None