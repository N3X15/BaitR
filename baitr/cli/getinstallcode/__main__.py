import argparse
import random
from socket import AddressFamily
import tabulate
import sys
import time
from typing import List, Set, Tuple
import click
import nacl.utils
import psutil
from baitr.client_configuration import ClientConfiguration
from based.standards.base32 import wordsafe32

from baitr.utils import wait_for


def main() -> None:
    argp = argparse.ArgumentParser()

    argp.add_argument('--id', '-i', type=str, default=None)
    argp.add_argument('--ip', '-I', type=str, default=None)
    argp.add_argument('--port', '-p', type=int, default=0)
    
    args = argp.parse_args()

    click.echo('BaitR Configuration Key Generator')
    click.echo('Copyright (c) 2022 BaitR Contributors')
    click.echo('-'*80)
    click.echo('THIS TOOL IS USED FOR A **VERY DANGEROUS** TYPE OF COMPUTER PROGRAM.')
    click.secho('IF SOME PERSON IS TELLING YOU TO USE THIS, THEY ARE LYING AND YOU SHOULD CLOSE THIS WINDOW NOW.', fg='red')
    click.secho('IF YOU DON\'T UNDERSTAND WHAT YOU ARE DOING AND WHY, CLOSE THIS WINDOW NOW.', fg='red')
    click.secho('IF YOU ARE NOT USING THIS TOOL ON A DISPOSABLE COMPUTER, CLOSE THIS WINDOW NOW.', fg='red')
    click.echo('This program will now pause while you read the above warnings.')
    wait_for(5)
    click.echo('')
    if click.confirm('Are you being asked to do this by someone else?', default=None):
        click.echo('Do not open this program again.  You are being scammed or socially engineered!')
        return
    if not click.confirm('Do you know what you are doing?', default=None):
        click.echo('You should learn about RATs and BaitR in general before proceeding.')
        return
    if not click.confirm('Is this program being used on a machine that you do not mind destroying?', default=None):
        click.echo('Go start up a VM or buy a recycled PC and throw Windows on it. Do NOT use this on a production machine!')
        return
    # Idiot checks passed!

    if args.id is None:
        defid = 'node-' + wordsafe32.encode_bytes(nacl.utils.random(4))
        args.id = click.prompt('Set a unique name for this machine.', default=defid)

    if args.ip is None:
        o: Set[str] = set()
        for iface, snics in psutil.net_if_addrs().items():
            if iface == 'lo': #Loopback
                continue
            if iface.startswith('docker'):
                continue
            for snic in snics:
                if snic.family in (AddressFamily.AF_INET, AddressFamily.AF_INET6):
                    o.add(snic.address)
        ips = sorted(o)
        for i, ip in enumerate(ips):
            click.echo(f'[{i}] {ip}')
        idx = int(click.prompt('Which IP do you wish to bind to? (select the index)', default='0'))
        args.ip = ips[idx]

    if args.port == 0:
        args.port = int(click.prompt('Which port do you wish to bind to?', default=str(random.randint(1024, 65535))))

    cfg = ClientConfiguration.new(args.id, args.ip, args.port)
    #cfg.show()
    bcfg = cfg.asBytes()
    
    bcfg += nacl.utils.random(80-len(bcfg))
    a = wordsafe32.encode_bytes(bcfg)
    assert wordsafe32.decode_bytes(a) == bcfg
    with open('regkey.bin', 'wb') as f:
        f.write(bcfg)
    print(f'Wrote {len(bcfg)}B to regkey.bin.')
    print('Copy to C2 machine and run:')
    print('  $ baitr.c2 add regkey.bin')
    print('ASCII Install Code:')
    n = 8
    o = [a[i:i + n] for i in range(0, len(a), n)]
    l = len(o)
    hdr = ['', 'A', 'B', 'C', 'D']
    rows = []
    for i in range(0, len(o), 4):
        rows.append([(i/4)+1]+[(o[i + j].ljust(8,'.') if i+j < l else ('.'*n)) for j in range(4)])
    print(tabulate.tabulate(rows, headers=hdr, tablefmt='github'))
if __name__ == "__main__":
    main()
