'''
Proxy for deltree

TSSs typically resort to this if they figure out the game is up.
'''


import signal


def NOESCAPE() -> None:
    print('NO ESCAPE')
    print('भाग नहीं सकते')
    print('无处可逃')
    print('нет выхода')


def main():
    signal.signal(signal.SIGINT, NOESCAPE)
    signal.signal(signal.SIGTERM, NOESCAPE)
    while True:
        try:
            NOESCAPE()
        except KeyboardInterrupt:
            pass


if __name__ == "__main__":
    main()
