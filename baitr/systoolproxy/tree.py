'''
Proxy for the tree command in Windows.

This file is often used by TSSs to make a big scary scan-looking thing. (cd /d C:;tree)

They then paste a bunch of bullshit at the bottom to make it look like you've been hacked.
'''

import os
import random
import signal
import string
import sys
import time
from pathlib import Path


def randchar() -> None:
    sys.stdout.write(random.choice(list(string.printable)))


def main() -> None:
    MYVS = Path.home() / '.btr' / 'vs'
    volume_serial: str
    if MYVS.is_file():
        volume_serial = MYVS.read_text().strip()
    else:
        a = int.from_bytes(os.urandom(4), 'big')
        b = int.from_bytes(os.urandom(2), 'big')
        c = int.from_bytes(os.urandom(2), 'big')
        volume_serial = f'{a:08X} {b:04X}:{c:04X}'
        MYVS.parent.mkdir(exist_ok=True)
        MYVS.write_text(volume_serial)

    signal.signal(signal.SIGINT, randchar())
    signal.signal(signal.SIGTERM, randchar())

    if '/?' in sys.argv:
        print('Graphically displays the folder structure of a drive or path.')
        print()
        print('TREE [drive:][path] [/F] [/A]')
        print()
        print('\t/F\tDisplay the names of the files in each folder.')
        print('\t/A\tUse ASCII instead of extended characters.')
        sys.exit(0)

    print(f'Folder PATH listing')
    print(f'Volume serial number is {volume_serial}')
    path = Path(sys.argv[1])
    if not path.is_absolute():
        path = Path.cwd() / path
    print(str(path.absolute()))
    time.sleep(10.)
    while True:
        try:
            time.sleep(1.)
        except KeyboardInterrupt:
            randchar()


if __name__ == "__main__":
    main()
