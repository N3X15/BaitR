'''
Proxy for netstat.

This file is often used by TSSs to make a big scary scan-looking thing.

They then paste a bunch of bullshit at the bottom to make it look like you've been hacked.
'''

import random
import signal
import string
import sys
import time


def randchar() -> None:
    sys.stdout.write(random.choice(list(string.printable)))

def main() -> None:
    if '/?' in sys.argv:
        print('Displays protocol statistics and current TCP/IP network connections')
        print()
        print('NETSTAT [-a] [-b] [-e] [-f] [-n] [-o] [-p proto] [-r] [-s] [-t] [-x] [-y] [interval]')
        sys.exit(0)

    signal.signal(signal.SIGINT, randchar())
    signal.signal(signal.SIGTERM, randchar())

    print('Active Connections')
    print('  Proto  Local Address        Foreign Address      State')
    time.sleep(10.)
    while True:
        try:
            time.sleep(1.)
        except KeyboardInterrupt:
            randchar()
if __name__ == "__main__":
    main()