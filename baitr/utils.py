import sys
import time
def wait_for(waitsec: int) -> None:
    for i in range(waitsec):
        towrite = str(waitsec - i)
        charlen = len(towrite)
        sys.stdout.write(towrite)
        sys.stdout.flush()
        time.sleep(1.)
        sys.stdout.write('\b'*charlen)
