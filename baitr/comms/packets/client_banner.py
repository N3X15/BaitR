from construct import Const, PascalString, Struct, VarInt, Bytes
HASH_SIZE: int = 32
ClientVersionInfo = Struct(
    'major' / VarInt,
    'minor' / VarInt,
    'patch' / VarInt
)

# Yes, this is blatant.  It's designed as such.
ClientBanner = Struct(
    'banner' / Const(b'BaitR Agent\n'),
    'proto_version' / VarInt,
    'baitr_version' / ClientVersionInfo,
)