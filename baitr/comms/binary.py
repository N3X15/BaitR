
import struct
from typing import BinaryIO


def write_u4(val: int, f: BinaryIO) -> None:
    f.write(struct.pack('>I', val))


def read_u4(f: BinaryIO) -> int:
    return struct.unpack('>I', f.read(4))[0]


def write_u8(val: int, f: BinaryIO) -> None:
    f.write(struct.pack('>Q', val))


def read_u8(f: BinaryIO) -> int:
    return struct.unpack('>Q', f.read(8))[0]


def write_str(val: str, f: BinaryIO) -> None:
    data = val.encode('utf-8')
    f.write(struct.pack('>I', len(data)))
    f.write(data)


def read_str(f: BinaryIO) -> str:
    return f.read(read_u4(f)).decode('utf-8')
