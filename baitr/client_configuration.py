from __future__ import annotations

import random
import socket
import uuid
from ipaddress import _IPAddressBase, ip_address
from typing import Any, Dict, Optional

import click
from based.standards.base32 import wordsafe32
from based.standards.base94 import base94
from construct import (Bytes, Const, PascalString, Rebuild, Struct, VarInt,
                       len_, this)
from nacl.public import PrivateKey
import psutil

MODEL_CCDATA = Struct(
    'version' / Const(b'\x02'),
    'id' / PascalString(VarInt, 'ascii'),
    'hostname' / PascalString(VarInt, 'utf8'),
    'ip' / VarInt,
    'port' / VarInt,
    'mac' / Bytes(6),
    'pk_length' / Rebuild(VarInt, len_(this.private_key)),
    'private_key' / Bytes(this.pk_length)
)

class ClientConfiguration:
    def __init__(self) -> None:
        self.id: str = ''
        self.ip_addr: _IPAddressBase = None
        self.pk: PrivateKey = None
        self.mac_addr: bytes = b''
        self.port: int = 0
        self.hostname: str = ''
        self.cores: int = 0

    @classmethod
    def new(cls, id: str, ip: str, port: int) -> ClientConfiguration:
        cli = ClientConfiguration()
        cli.id = id
        cli.port = port
        cli.hostname = socket.gethostname()
        cli.ip_addr = ip_address(ip)
        cli.cores = psutil.cpu_count(logical=False)
        cli.pk = PrivateKey.generate()
        cli.mac_addr = uuid.getnode().to_bytes(6, 'big')
        return cli

    def asDict(self) -> dict:
        return {
            'id': self.id,
            'ip': str(self.ip_addr),
            'p': self.port,
            'pk': bytes(self.pk),
            'ma': self.mac_addr,
            'ho': self.hostname,
            'co': self.cores,
        }

    def fromDict(self, data: Dict[str, Any]) -> None:
        self.id = data['id']
        self.ip_addr = ip_address(data['ip'])
        self.port = data['p']
        self.pk = PrivateKey(bytes(data['pk']))
        self.mac_addr = data['ma']
        self.hostname = data['ho']
        self.cores = data['co']

    def show(self) -> None:
        click.echo(f'Client {self.id}')
        click.echo(f'  ID:    {self.id}')
        click.echo(f'  Host:  {self.hostname}')
        click.echo(f'  IP:    {self.ip_addr}')
        click.echo(f'  Port:  {self.port}')
        click.echo(f'  Cores: {self.cores}')
        click.echo(f'  MAC:   {self.mac_addr.hex(":")}')
        click.echo(f'  PK:    {wordsafe32.encode_bytes(bytes(self.pk))}')

    def asBytes(self) -> bytes:
        return MODEL_CCDATA.build(dict(
            id=self.id, 
            hostname=self.hostname, 
            ip=int(self.ip_addr),
            port=self.port,
            mac=self.mac_addr,
            private_key=bytes(self.pk)))

    def fromBytes(self, b: bytes) -> None:
        data: Any = MODEL_CCDATA.parse(b)
        self.id = data.id
        self.hostname = data.hostname
        self.ip_addr=ip_address(data.ip)
        self.port = data.port
        self.mac_addr = data.mac
        self.pk = PrivateKey(data.private_key)
