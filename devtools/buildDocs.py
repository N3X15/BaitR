from pathlib import Path
from typing import List
from buildtools import os_utils, log
from buildtools.maestro import BuildMaestro
from buildtools.maestro.base_target import SingleBuildTarget

NODE_MODULES_DIR = Path.cwd() / 'node_modules'

ENV = os_utils.ENV.clone()
ENV.prependTo('PATH',  str(NODE_MODULES_DIR / '.bin'))

YARN = os_utils.assertWhich('yarn')

assert NODE_MODULES_DIR.is_dir(), 'Run `yarn` first.'

BYTEFIELD = ENV.assertWhich('bytefield-svg')

class BytefieldSVGTarget(SingleBuildTarget):
    BT_LABEL: str = 'BYTEFIELD-SVG'
    def __init__(self, infile: Path, outfile: Path, dependencies: List[str] = []) -> None:
        super().__init__(target=str(outfile), files=[str(infile)], dependencies=dependencies)
        self.infile: Path = infile
        self.outfile: Path = outfile

    def build(self) -> None:
        self.outfile.parent.mkdir(parents=True, exist_ok=True)
        os_utils.cmd([BYTEFIELD, '-s', str(self.infile), '-o', str(self.outfile)], echo=self.should_echo_commands(), env=ENV, show_output=True, critical=True)

def build_edn(bm: BuildMaestro, basename: str) -> str:
    bm.add(BytefieldSVGTarget(BYTEFIELDS_DIR / 'files' / f'{basename}.edn', BYTEFIELDS_DIR / 'svg' / f'{basename}.svg'))

BYTEFIELDS_DIR = Path('doc') / 'bytefields'
bm = BuildMaestro()
build_edn(bm, 'gickey')
bm.as_app()