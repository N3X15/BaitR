import ast
from pathlib import Path
from typing import List, Union

import python_minifier
from buildtools.maestro import BuildMaestro
from buildtools.maestro.base_target import SingleBuildTarget
from buildtools.maestro.enumwriters.python import PythonEnumWriter
from buildtools.maestro.genenum import GenerateEnumTarget
from buildtools.maestro.nuitka import NuitkaTarget

BAITR_SRC = Path('baitr')
DATA_DIR = Path('data')
TMP_DIR = Path('.tmp')

STUBS_SRC_DIR = BAITR_SRC / 'systoolproxy'
STUBS_TMP_DIR = TMP_DIR / 'stubsrc'
ENUM_PY_DIR = BAITR_SRC / 'enums'
ENUM_YML_DIR = DATA_DIR / 'enums'

class PyminiBuildTarget(SingleBuildTarget):
    BT_LABEL = 'PYMINI'
    def __init__(self, target: str, source: Union[str, Path], dependencies: List[str] = []) -> None:
        self.infile: Path = Path(source)
        self.outfile: Path = Path(target)
        super().__init__(str(target), [str(source)], dependencies)

    def build(self) -> None:
        i = self.infile.read_text()
        o=python_minifier.minify(i, str(self.infile))
        self.outfile.write_text(o)

bm = BuildMaestro()
pywriter = PythonEnumWriter()
enums = [
    bm.add(GenerateEnumTarget(str(ENUM_PY_DIR / 'datatype.py'), str(ENUM_YML_DIR / 'datatype.yml'), pywriter)).target,
    bm.add(GenerateEnumTarget(str(ENUM_PY_DIR / 'floatflags.py'), str(ENUM_YML_DIR / 'floatflags.yml'), pywriter)).target,
]

bm.as_app()
